from imsi_cli import imsi_logger
from imsi_setup import setup
from imsi_standalone_commands import config, reload, build, submit
from imsi_commands import list, set

import click
import os
from pathlib import Path

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])

@click.group(invoke_without_command=True, context_settings=CONTEXT_SETTINGS)
@click.option("--force", is_flag=True, help="Force the command to run")
@click.pass_context
def cli(ctx: click.Context, force: bool) -> None:
    """
    Entry point for imsi CLI. When running `imsi setup ...`, a temporary
    version of imsi is cloned locally from which the main source code version of setup_run is called from the uim.
    This code serves as an interface layer and will periodically need to be updated to reflect changes to the imsi uim.
    """
    wrk_dir = Path(os.getenv("WRK_DIR", "."))

    ctx.ensure_object(dict)
    # These context objects simply store these variables
    # and pass them to downstream click commands
    ctx.obj["force"] = force
    ctx.obj["src"] = wrk_dir / "src"
    ctx.obj["imsi_subtree_src"] = Path("imsi-config") / "imsi-src"
    ctx.obj["imsi_src_path"] = ctx.obj["src"] / ctx.obj["imsi_subtree_src"]

    if ctx.invoked_subcommand != "setup" and not ctx.obj["src"].exists():
        logger.error(
            "⚠️ 'src' directory not found! This is likely caused by:\n"
            "1. You are not currently in your setup directory or one hasn't been created.\n"
            "    or \n"
            "2. The environment variable WRK_DIR is not set to the correct directory."
        )
        exit(1)

    if ctx.invoked_subcommand != "setup":
        # a separate setup log is created within the setup function.
        logger = imsi_logger.setup_logger(__name__, log_path=wrk_dir)
        logger.info(f"imsi {ctx.invoked_subcommand} invoked")


# Add commands to the CLI group
commands = [setup, config, reload, build, submit, list, set]
for command in commands:
    cli.add_command(command)

if __name__ == "__main__":
    cli()
