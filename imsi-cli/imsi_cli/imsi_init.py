from imsi_cli import imsi_logger, path_tools
from typing import Tuple
import pathlib
import sys
import functools
import sys
import click


def with_uim(func):
    @click.pass_context
    @functools.wraps(func)
    def wrapper(ctx, *args, **kwargs):
        sys.path.insert(0, str(ctx.obj["imsi_src_path"].resolve()))
        from imsi.user_interface import ui_manager as uim
        return func(ctx, uim, *args, **kwargs)
    return wrapper
