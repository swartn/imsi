====================================================================
Developing or modifying configurations (models/experiments/machines)
====================================================================


Adding new options for future reuse
------------------------------------

Options allow for selection from a range or choices, similar to what would be provided by an if statement 
or switch in scripting. In the `imsi-config` directory of the source code, modify the 
`models/model_options.json`, and add options following the examples provided. Any imsi setting can 
be modified via this mechanism. 

Adding new models or experiments
--------------------------------

You must create a new model or experiment file in the models `imsi_config` directory. Start from an 
experiment/model that is as close to your target experiment as possible and copy and edit its json file.
Use the `inherits_from` functionality to point to this experiment as the parent. Then modify ONLY
what you have to create your new model/experiment. Using inheritance in this way reduces repetition, 
and ensure that if a setting is altered in the parent configuration, that it is propagated into yours.

The `CanESM Changes Tutorial <https://gitlab.com/CP4C/cp4c-docs/-/blob/main/cp4c-tutorial-apr-2024/canesm_changes_tutorial.ipynb>`_
provides some direct examples of creating a new experiment.

Porting an imsi based model to a new machine
--------------------------------------------

Add a new machine, and compiler consideration for that machine in the `imsi-config/machines` directory.
You will need to figure out the details about required variables, models, scratch space to use, and 
you will need to download the model forcing files, and create the required DATAPATH_DB input database.
As for other additions, it is easiest to start from the most closely related machine and if possible,
use `inherits_from`.

Using imsi with a different model alltogether
---------------------------------------------

While imsi was originally create to configure models from the CCCma Integrated Modelling System,
it can be used to configure any model in principle. The key requirements are to create the 
`imsi-config` directory at the top level of the model code repository, and populate it with
the relavant `json` files that describe the configuration of the model. 

The imsi code itself has no knowledge about the underlying model. All model specific information
is injected through the json configuration.
