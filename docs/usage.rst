=====
Usage
=====

Configuring user defaults
-------------------------

The file

.. code-block:: bash

    ~/.imsi/imsi-user-config.json

can be used to set user preferences and options. These are always respected over any
other choices [Work in Progress].


Setting up a run
----------------

Using `imsi setup` to create a run directory, obtain the model source code,
and extract all required model configuration files. The setup will result
in an imsi configured directory, from which futher interaction with the run
is conducted. 

The output of `imsi setup -h`:

.. program-output:: imsi-src-cli setup -h

The imsi setup creates some subdirectories for the run, including `bin` (for executables),
and `config`. The `config` directory contains various run configuration files, including
cpp directives and namelists, that have been extracted and modified according to the choice
of model and experiment.

The setup also creates a log file (`.imsi-setup.log`), and a readable/writable json state file containing the 
full details of the configuration options 
(`imsi_configuration_<runid>.json`). A `save_restarts_files.sh` is extracted to allow saving of files to
initiate the run. This must be executed before `imsi submit`.

Example
+++++++

.. code-block:: bash

    imsi setup --repo=https://gitlab.com/cccma/canesm --ver=develop_canesm --exp=cmip6-piControl --model=canesm51_p1 --runid=<unique-runid>


Quering available configurations
--------------------------------

Use `imsi list` to query the supported machines, compilers, models and experiments
in a given imsi repository. If you are already in an `imsi` configured directory,
it will be based on the local model repository. If you are not in an imsi-configured
directory, you can point towards the relevant model directory to scan for supported
options (using `--repo`).

The output of `imsi list -h`:

.. program-output:: imsi-src-cli list -h


Example
+++++++

.. code-block:: bash

    imsi list -e --repo=/home/$USER/canesm

        Supported experiments: cmip6-1pctCO2 cmip6-abrupt-4xCO2 cmip6-piControl cmip6-amip cmip6-historical 

Modifying basic run parameters
------------------------------

To modify an run paramters, there are basically 4 choices, in order of preference:

1. Use `imsi set` to apply a specific option or selection (see set usage below)

2. Modify the upstream `.json` files in the `imsi-config` directory (of the model source code in the run directory) 
   and change settings as required. Once complete, run `imsi reload`, which will reparse the source and update the
   configuration. Modifying in such a way is highly desirable, because any changes can easily be commited, reused and 
   shared. Any major development of new experiments/configurations should be done directly in the jsons in this way.

3. Modify the `imsi_configuration_<runid>.json` file, and run `imsi config`. This will ingest the json file and apply
   all of its settings into the current configuration. This is is the appropriate way to alter run-specific things 
   like `parent_runid`. However, it is not desirable for general development of configurations as it is easy to make 
   inconsistent changes and any changes are not easily resuable. 

4. Edit files in the `config` directory directly. The files in the `config` directory are the "single source of truth", in other
   words, they are the files that will be used by the model simulation. While editing them directly might be convenient for fast 
   tests, the changes are not easily captured or resused, and this method is prone to internal consistency errors.

The `CanESM Changes Tutorial <https://gitlab.com/CP4C/cp4c-docs/-/blob/main/cp4c-tutorial-apr-2024/canesm_changes_tutorial.ipynb>`_
provides some direct examples of making common changes like altering parent_runid and simulation dates.

Using imsi set
++++++++++++++

Which selections can be set are given by `imsi list` (as above).

The output of `imsi set -h`:

.. program-output:: imsi-src-cli set -h


Example
+++++++

Choose the configuration using 2+4 nodes (atm-ocean):

.. code-block:: bash

    imsi set -o pe_config=2+4

Example
+++++++

Change the experiment you selected during setup:

.. code-block:: bash

    imsi set -s experiment_name=cmip6-historical    

This will reconfigure the setup for the historical experiment. Similarly you could change
`machine_name`, `model_name` or `compiler_name`.

Building run executables
------------------------

The output of `imsi build -h`:

.. program-output:: imsi-src-cli build -h

Example
+++++++

.. code-block:: bash

    imsi build

Saving restarts
---------------

.. code-block:: bash

    ./save_restart_files.sh

Submitting the run
------------------

.. code-block:: bash

    imsi submit


