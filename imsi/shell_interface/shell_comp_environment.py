 # Pure test to see if we can use the config_manager to do something.
# i.e. create a way to use the config manager in downstream utilities

from imsi.config_manager.config_manager import Configuration
from imsi.utils.dict_tools import flatten
from imsi.utils.general import get_date_string
import copy
import collections

# t could be asked whether a template in the imsi input database (jsons) could be
# used to inform this writing, rather than implicitly constructing a template through code below.
# Some things here might be more appropriate in config_manager. In a sense they have been
# brought out here as they were messy, and hence, might change in future, but that might not
# be a logical way to proceed. 

def generate_computational_environment(configuration: Configuration):
    """Generate content of a file that can be sourced in a shell
       to set the computational environment for compiling and running the model.

       Here we just generate and return a list of strings, that will be written
       to file elsewhere using a common tool.
    """
    machine_config = copy.deepcopy(configuration.machine.parameters)
    compiler_name = configuration.compiler.name

    comp_env_content = list()
    # There is an issue with the below, in that the unordered dict can put
    # e.g. module purge after the module load sections. Ordereddict might be necessary
    # -> were in imsi v0.1.
    comp_env_content.append("# Imsi created model environment file for compiling and running\n")
    datestr = get_date_string()
    comp_env_content.append(f"# Created for {configuration.machine.name} on date: {datestr}")
    comp_env_content.append("\n# Module definitions")
    # The below code combines the module actions under "all" with those under the specific
    # compiler being used and writes these out to the file.
    # It is important NOT to change the order of the keys below, as module command
    # order is important. 
    # ---
    # This is definitely a "configuration" step, not an interface step!
    if compiler_name in machine_config['modules']:
        module_config_compiler = machine_config['modules'][compiler_name]
    else:
        module_config_compiler = {}
    #---    
    module_config =  machine_config['modules']['all']
    if module_config:
        # Surely there is a better way than this V!
        # Combine the lists of keys, strictly preserving the order of the
        # all keys list. Normally I would use a set() if order was not important.
        compiler_keys = list(module_config_compiler.keys())
        all_keys = list(module_config.keys())
        for k in all_keys:
            if k in compiler_keys:
                compiler_keys.remove(k)
        module_cmd_keys = all_keys + compiler_keys
        # Here we append the strings together.
        # Note that this is purely additive. We might consider changing the machine
        # config to have an "append" feature, for appending or overwriting, as is done
        # for the compiler_config using OrderedDicts.
        for modcmd in module_cmd_keys:
            argstr = ""
            if modcmd in module_config_compiler.keys():
                argstr += " ".join(module_config_compiler[modcmd])
            if modcmd in module_config.keys():
                argstr += " " + " ".join(module_config[modcmd])
            comp_env_content.append(f'module {modcmd} {argstr}')
    comp_env_content.append("\n# Environment variables")
    env_variable_config =  machine_config['environment_variables']['all']
    for k,v in env_variable_config.items():
        comp_env_content.append(f'export {k}={v}')
    comp_env_content.append("\n # Environment commands")
    env_command_config =  machine_config['environment_commands']['all']
    for k,v in env_command_config.items():
        comp_env_content.append(f'{k} {v}')
    return comp_env_content

def generate_compilation_template(configuration: Configuration):
    """Generate the compilation_template file contents based on a Configuraiton instance

    Here we just generate and return a list of strings, that will be written
    to file elsewhere using a common tool.
    """
    machine_name = configuration.machine.name
    compiler_name = configuration.compiler.name
    compiler_config =  copy.deepcopy(configuration.compiler.parameters)
    comp_env_content = list()

    comp_env_content.append("# Imsi created model environment file for compilation\n")
    datestr = get_date_string()
    comp_env_content.append(f"# Created for the compiler: {compiler_name} on machine: {machine_name} on date: {datestr}")
    comp_env_content.append("\n#Compiler flag options")
    for language, lang_config in compiler_config.items():
        comp_env_content.append(f'\n#{language} options')
        if isinstance(lang_config, collections.abc.MutableMapping):
            flat_lang_config = flatten(lang_config)
            for k,v in flat_lang_config.items():
                if isinstance(v, list):
                    argstr=' '.join(v)
                else:
                    argstr=v
                comp_env_content.append(f'{k}={argstr}')
    return comp_env_content

if __name__ == "__main__":
    pass