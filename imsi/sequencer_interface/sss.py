from typing import List, Optional
from imsi.scheduler_interface.schedulers import Scheduler, BatchJob
from imsi.utils.general import get_date_string
from dataclasses import dataclass, field, asdict
from collections import OrderedDict
import subprocess
from imsi.utils.general import write_shell_script
import imsi
import os
import json

# You could make these more elaborate classes, and essentially try to build a more feature rich
# sequencer, but that is not really the point. SSS is only mean for simple testing really.
@dataclass
class SimpleShellSequencer:
    ''' A class to contain the top level information of a sequencer'''
    runid: str 
    run_config_dir: str          
    sequencing_scratch_dir: str 
    work_dir: str 
    scheduler: Scheduler              
    scheduler_submission_command: str # e.g. qsub or sbatch
    # for start and end, they are labelled "time" and now time is passed. 
    # But usage below expects year. Will break if time is specified to higher precision.
    start_time: str  # logically belongs more in jobs, but KISS here
    end_time: str    
    # All jobs handled by this scheduler.
    jobs: OrderedDict = field(default_factory=OrderedDict)    
    resubmit_flags: list = field(default_factory=list)
    time_counter_names: list = field(default_factory=list)

    def __post_init__(self):
        self.timing_parameters_file_fullpath = os.path.join(self.run_config_dir, '.simulation.time.state')

    def add_job(self, job):
        """Add a job to the jobs OrderedDict."""
        if not isinstance(job, SSSjob):
            raise TypeError("job must be an instance of SSSjob")
        self.jobs[job.job_identifier] = job
        
        # Add the cycle flag to be used later in .simulation.time.state
        if job.cycle_flag:
            self.resubmit_flags.append(job.cycle_flag)
        if job.time_counter:
            self.time_counter_names.append(job.time_counter)

    def configure(self):
        '''Do the on disk setup of required files'''
        
        # timing parameters file
        self.write_time_parameters(self.start_time, self.end_time, parameters_file=self.timing_parameters_file_fullpath)

        for jobname, job in self.jobs.items():
            script_content = self.generate_simple_shell_sequencing_content(**asdict(job))
            # now need to create/write the batch job using scheduler module
            directives=job.directives
            if not directives:
                directives = self.scheduler.default_directives # backwards compatibility. Don't love the coupling here!
            directives = directives + [self.scheduler.output_redirect.format(PATH=job.listing_prefix)] # Add the redirect of listings to directives > to improve.
            batch_job = BatchJob(user_script=script_content, job_directives=directives)
            batch_script = batch_job.construct_job_script(self.scheduler)
            write_shell_script(job.submission_script_fullpath, batch_script, mode='w')
        
    def run(self): 
        """
        Submits the first batch job in the list to the queue. 
        """
        os.chdir(self.work_dir)
        job_identifier, job = self.jobs.popitem(last=False)
        submission_fullpath = job.submission_script_fullpath
        # Should use the scheduler submit command.
        subprocess.run(self.scheduler_submission_command.split() + [submission_fullpath])

    def write_time_parameters(self, start_time: str, end_time: str, parameters_file: str='.simulation.time.state', timing_cycle_flags: list=[]):
        # All jobs have this
        header_content = [
            "# Imsi created shell timing file",
            f"START_YEAR={start_time}", # Should not map time into year. Works now but will break. Either use time or input the year.
            f"END_YEAR={end_time}",
        ] 
        time_counters = [f'{counter}={start_time}' for counter in self.time_counter_names] # Add the individual cycling flags
        resubmit_flags = [f'{flag}=FALSE' for flag in self.resubmit_flags] # Add the individual cycling flags
        
        # Join these three lists to write.
        time_state_content = header_content + time_counters + resubmit_flags
        # Do the write to file
        write_shell_script(parameters_file, time_state_content, mode='a')

    def generate_simple_shell_sequencing_content(self,
                                        submission_script_fullpath, 
                                        job_identifier, 
                                        user_script_to_source=None, 
                                        dependencies=None, 
                                        cycle_flag="IMSI_RESUBMIT", 
                                        time_counter="CURRENT_YEAR",
                                        clean_scratch=True,
                                        **kwargs,
                                        ) -> List:
        """
        V Docstring out of date V
        Generate submission file content for the simple shell sequencer,  with depenedcy and resubmission logic, relying on 
        config/.simulation.time.state time tracking.  
        Inputs:
            script_name : str
                name / path of the file to write
            job_identifier : str
                string to insert into the job name / scratch directory name
            user_script_to_source : str
                name/fullpath of the script to run
            resubmit_from : path
                A location to do the submission from

        This is effectively a bootstraped imsi/shell sequencer. 
        """
        # This creates a list, whose content reflects the core sequencing logic of SSS

        datestr = get_date_string()

        content = [
            "#!/bin/bash",
            "\n# Imsi created shell environment file"
            f"# Simple Shell Sequencer Created on date: {datestr}", # add machine, compiler, experiment, model info?

            # source required input information regarding env vars, settings and timers.
            f"source {self.run_config_dir}/computational_environment",
            f"source {self.run_config_dir}/shell_parameters",
            f"source {self.run_config_dir}/.simulation.time.state",    

            # Create and go to the run scratch directory
            "\n# Create a scratch directory\n",                                              
            f"IMSI_SCRATCH={self.sequencing_scratch_dir}/{self.runid}/{self.runid}_scratch_{job_identifier}_${{{time_counter}}}_$$",
            "mkdir -p $IMSI_SCRATCH\n",
            "cd $IMSI_SCRATCH\n\n",   

            # Run the logic script provided 
            "# User defined scripting",
            f"source {user_script_to_source}\n",

            # Go back to the run directory
            "# Wrap and job cycling",
            f"cd {self.work_dir}",
        ]

        # Resubmit if necessary
        if cycle_flag:        
            content += [
                # Source timing file to determine restartability. The run script above would have altered this.
                f"source {self.run_config_dir}/.simulation.time.state",
                f'if [ "${cycle_flag}" == "TRUE" ] ; then',
                f"   {self.scheduler_submission_command} {submission_script_fullpath}",
                'fi',
            ]
        # Submit dependencies if they exist:
        if dependencies:
            for dep in dependencies:
                content += [
                    f"   {self.scheduler_submission_command} {dep}"
                ]
        if clean_scratch:
            content += [
            "rm -rf $IMSI_SCRATCH"
            ]
        return content
    
    def to_json(self) -> str:
        # Convert to dictionary, handling OrderedDict conversion
        data = asdict(self)
        data['jobs'] = {k: asdict(v) for k, v in self.jobs.items()}
        return json.dumps(data, indent=4)
    
    def save_to_json(self, file_path: str):
        json_str = self.to_json()
        with open(file_path, 'w') as file:
            file.write(json_str)

    @staticmethod
    def from_json(json_str: str) -> 'SimpleShellSequencer':
        data = json.loads(json_str)
        data['jobs'] = OrderedDict((k, SSSjob(**v)) for k, v in data['jobs'].items())
        return SimpleShellSequencer(**data)

    @staticmethod
    def load_from_json(file_path: str) -> 'SimpleShellSequencer':
        with open(file_path, 'r') as file:
            json_str = file.read()
        return SimpleShellSequencer.from_json(json_str)
       
@dataclass
class SSSjob:
    ''' A class to contain an individal job run in the sequencer'''
    submission_script_fullpath: str   # Name/path of the batch job script 
    job_identifier: str               # Job name to be used in queue/listings/scratch dirs
    user_script_to_source: str        # This is an extant shell script that will be sourced within this job.
    directives: list = field(default_factory=list)                  # These are directives for the scheduler, like wallclock, cores, etc
    listing_prefix: str ='output'              # Path/prefix name where to place job listings.
    dependencies: list = field(default_factory=list)                # A list of other jobs that this job should submit
    cycle_flag: str = ''            # Does this job resubmit itself?
    time_counter: str = ''          # The name of the counter to use for this job in the time tracking file
    clean_scratch: bool = True        # Remove the scratch directory if the job completes

## Not used:
def __write_path_configration(self):
    def write_mk_and_link(path, f):
        f.write(f"mkdir -p {path}\n")
        f.write("mkdir -p ${WRK_DIR}/hub ; cd ${WRK_DIR}/hub\n")
        f.write(f"ln -s {path} .\n")
    with open(f'path_configuration.sh', 'w') as f: 
        f.write("# Imsi created path config file\n")
        datestr = get_date_string()
        f.write(f"# Created for machine: {self.sim.machine} on date: {datestr}\n")
        write_mk_and_link("$EXEC_STORAGE_DIR", f)
        write_mk_and_link("$RUNPATH", f)
