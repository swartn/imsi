import cftime
from imsi.time_manager import cftime_utils 
from dataclasses import dataclass, field
from typing import Type
from datetime import timedelta
import xarray

@dataclass
class SimDateTime:
    """
    Base class for representing an individual time point.
    
    Attributes:
        time_string (str): The time string in ISO 8601 format.
        calendar (str): The calendar type.
        time (cftime.datetime): Parsed cftime datetime object.
        cftime_calendar (Type[cftime.datetime]): cftime calendar class.
    """
    time_string: str
    calendar: str = 'noleap'
    time: cftime.datetime = field(init=False)
    cftime_calendar: Type[cftime.datetime] = field(init=False)

    def __post_init__(self):
        self.cftime_calendar = cftime_utils.get_date_type(self.calendar)

@dataclass
class StartDateTime(SimDateTime):
    """Class for handling the start time of a simulation."""
    
    def __post_init__(self):
        super().__post_init__()
        self.time = parse_time(self.time_string, self.cftime_calendar, date_type='start')

@dataclass
class StopDateTime(SimDateTime):
    """Class for handling the stop time of a simulation."""
    
    def __post_init__(self):
        super().__post_init__()
        self.time = parse_time(self.time_string, self.cftime_calendar, date_type='end')

def parse_time(time_str: str, cftime_calendar: Type[cftime.datetime], date_type: str) -> cftime.datetime:
    """
    Parse a time string in ISO 8601 format and return a cftime datetime object
    for the specified calendar type and date_type.
    Args:
        time_str (str): The time string to parse.
        cftime_calendar (Type[cftime.datetime]): cftime calendar class.
        date_type (str): Specifies if the date is a 'start' or 'end' date.
    
    Returns:
        cftime.datetime: The parsed datetime object for the specified calendar.
    
    Raises:
        ValueError: If time_str is empty or date_type is invalid.
    """
    if not time_str:
        raise ValueError("Time string must be provided")
    parsed, resolution = cftime_utils._parse_iso8601_with_reso(cftime_calendar, time_str)
    start, end = cftime_utils._parsed_string_to_bounds(cftime_calendar, resolution, parsed)
    
    dates = {'start': start, 'end': end}
    
    try:
        return dates[date_type]
    except KeyError:
        raise ValueError(f'date_type must be one of "start" or "end". Received: {date_type}')

def generate_job_start_end_date_lists(start: cftime.datetime, stop: cftime.datetime, duration: str, 
                                      calendar: str):
    '''
        Get an array of job start and stop dates, given "segement" start and stop dates, and a duration
        of chunking.
        Uses xarray date range generation, and frequency conventions
    Args:
        start (cftime.datetime): The starting time.
        stop (cftime.datetime): The ending time.
        duration (str): The duration specifying the step size.
        calendar (str): The calendar (normally 'noleap')
    Returns:
        (job_start_array, job_end_array)  (tuple): tuple of lists containing cftimes of job start/ends
    '''
    # use xarray to generate a cftime_range. This requires quite a few functions/classes to achieve,
    # so they we not replicated here (but could be considered for stability and to remove xarray dependency,
    # which is probably an overkill dependency since only some cftime functionality is being used)
    #offset = cftime_offsets.to_offset(frequency)
    #return list(cftime_offsets._generate_range(start, end, None, offset))
    #nchunks = len(all_starts_times)
    #return nchunks
    # We must adjust the start, to handle cases, for example:
    # start='2000-01-02', end='2000-12-31'
    # which will lead to the range starting at '2000-02-01' if frequency is 'MS'  
    adjusted_start = cftime_utils.adjust_start_date(start, duration) # this function should be replaced as it is limited
    job_starts = list(xarray.cftime_range(adjusted_start, stop, periods=None, freq=duration, calendar=calendar))
    job_starts[0] = start # We want the first element to be the actual specified start; All other fields will fall on a period boundary
    job_ends = list(xarray.cftime_range(adjusted_start, periods=len(job_starts) + 1, 
                                        freq=duration, calendar=calendar) 
                        - timedelta(seconds=1) # shift back 1 second from start times to get end times
                    )[1::]                     # Do not use the first element (not an end time)
    job_ends[-1] = stop # We want the last element to be the actual specified end; 
    return (job_starts, job_ends)


def example_usage():
    run_dates = {
        'run_start_time': '6000-01-05',
        'run_stop_time': '6002-12',
        'run_segment_start_time': '6000-01-01',
        'run_segment_stop_time': '6009-12',
        'model_chunk_size': '12MS',
        'model_internal_chunk_size': '12MS',
        'postproc_chunk_size': '12MS',
    }
    calendar = run_dates.get('calendar', 'noleap')

    run_start_time = StartDateTime(time_string=run_dates.get('run_start_time'), calendar=calendar).time
    run_stop_time = StopDateTime(time_string=run_dates.get('run_stop_time'), calendar=calendar).time

    run_segment_start_time = StartDateTime(time_string=run_dates.get('run_segment_start_time'), calendar=calendar).time
    run_segment_stop_time = StopDateTime(time_string=run_dates.get('run_segment_stop_time'), calendar=calendar).time

    model_chunk_size = run_dates.get('model_chunk_size')
    model_internal_chunk_size = run_dates.get('model_internal_chunk_size')
    postproc_chunk_size = run_dates.get('postproc_chunk_size')

    (model_submission_job_start_list, 
     model_submission_job_end_list) = generate_job_start_end_date_lists(start=run_segment_start_time, 
                                                                        stop=run_segment_stop_time, 
                                                                        duration=model_chunk_size,
                                                                        calendar=calendar)
    (model_execution_loop_start_list,
     model_execution_loop_end_list) = generate_job_start_end_date_lists(run_segment_start_time, 
                                                                        run_segment_stop_time, 
                                                                        model_internal_chunk_size,
                                                                        calendar=calendar)
    
    (postproc_submission_job_start_list,
     postproc_submission_job_end_list) = generate_job_start_end_date_lists(run_segment_start_time, 
                                                                           run_segment_stop_time, 
                                                                           postproc_chunk_size,
                                                                           calendar=calendar)
    
    for (s,e) in zip(model_submission_job_start_list, model_submission_job_end_list):
        print(s, '   --   ', e) 

if __name__ == '__main__':
   example_usage()
