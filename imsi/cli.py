"""
imsi CLI
--------

The entry-point console script that interfaces all users commands to imsi.

imsi has several categories of sub-commands. As this module develops further,
the sub-parsers will be implemented in the relevant downstream modules.

"""
import argparse
import sys
import logging
from imsi.user_interface import ui_manager as uim

# Set up logging configuration
logging.basicConfig(
    filename=".imsi-setup.log",
    level=logging.INFO,
    format='%(asctime)s - SETUP - %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S %Z'
)

def main():
    """Console script for imsi."""
    # we might consider splitting this into multiple places. i.e. specific files for each major option. Still united
    # and accessed through this single common interface though for sure.
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--force', action='store_true', help='Force the operation (for config/reload)')
    parser.add_argument('--log', action='store_true', default=True, help='Log the setup command')
    subparsers = parser.add_subparsers(dest='command')
    
    setup = subparsers.add_parser('setup', help='Setup the run directory and clone source code')
    setup.add_argument('--runid', required=True, help='The name of the run to use. Typically a unique short string, not including "_" or special chars.')
    setup.add_argument('--repo', default='git@gitlab.com:cccma/canesm.git', help='url or path or repository to clone')
    setup.add_argument('--ver', default='develop_canesm', help='Version of the code to clone (commit, branch, tag...')
    setup.add_argument('--exp', default='cmip6-piControl', help='The experiment to run, contained under experiments directory in CanESM/CONFIG')
    setup.add_argument('--model', default=None, help='Optional. The model run, contained under model in CanESM/CONFIG. Can be changed later.')
    setup.add_argument('--fetch_method', default='clone', help='Defaults to "clone". Optionally specify "link" or "copy" to softlink or copy the code from disk instead of cloning')
    setup.add_argument('--seq', default=None, help='Sequencer to use. Valid options are "iss" and "maestro". If not provided, uses machine default.')
    setup.add_argument('--machine', default=None, help='Optionally specify the machine to use. If not specified, infer machine from hostname.')
    setup.add_argument('--flow', default=None, help='Optionally specify the sequencing flow to use. If not specified, use machine defaults specified in machine/model.')

                # Note the --seq arg could be modified to take additional args, e.g. for the maestro repo / ver

    config = subparsers.add_parser('config', help='Configure a simulation, using updated settings from the imsi_configuration_${runid}.json file')
    reload = subparsers.add_parser('reload', help='Reload the imsi configuration from the on-disk repo jsons and update the simulation configuration')
    build = subparsers.add_parser('build', help='Build a simulation')
    submit = subparsers.add_parser('submit', help='Submit a simulation to run')

    list = subparsers.add_parser('list', help='List available configuration selections')
    list.add_argument('-r', '--repo', required=False, default=None, help='Model code directory containing imsi-config subdirectory with imsi json configurations')
    list.add_argument('-e', '--experiments', action='store_true', required=False, help='Supported experiment names')
    list.add_argument('-m', '--models', action='store_true', required=False, help='Supported model names')
    list.add_argument('-p', '--platforms', action='store_true', required=False, help='Supported machine names')
    list.add_argument('-c', '--compilers', action='store_true', required=False, help='Supported compiler names for this machine')
    list.add_argument('-o', '--options', nargs='?', default=False, const="no_opt", required=False, help='Available model options')
    
    #summary = subparsers.add_parser('summary', help='Print summary of a current imsi simulation configuration')
    #summary.add_argument('-d', '--detail', required=False, default=0, type=int, help='Integer level of detail for summary (1-3)')

    set = subparsers.add_parser('set', help='Set an imsi selection in the simulation configuration')
    set.add_argument('-f', '--file', required=False, help="The name of a json file containing the imsi selections.")
    set.add_argument('-s', '--selections', required=False, metavar="KEY=VALUE", nargs="+", help="A series of KEY=VALUE selection pairs to apply to the imsi simulation configuration")
    set.add_argument('-o', '--options', required=False, metavar="KEY=VALUE", nargs="+", help="A series of KEY=VALUE optin pairs to apply to the imsi simulation configuration")

    #qstat = subparsers.add_parser('qstat', help='imsi formatted qstat')
    #qstat.add_argument('-u', '--user', default=None, required=False, help="The name of a user to search queue by.")

    args = parser.parse_args()

    if args.command == 'setup':
        allowed_methods = {'clone', 'link', 'copy'}
        if args.fetch_method not in allowed_methods:
            print('\n\n**ERROR**: --fetch_methods must be one of "clone", "link" or "copy"\n')
            setup.print_help()
            return  # Assuming we want to exit the function here -- guard clause

        uim.setup_run(
            args.runid, args.repo, args.ver, args.exp, args.model,
            args.fetch_method, seq=args.seq, machine=args.machine, flow=args.flow
        )
        # Only log if setup completes successfully
        if args.log:
            logging.info(f"IMSI setup for {args.runid}")
            logging.info(' '.join(sys.argv))
            logging.info("🎉 🎉 🎉 🎉")  # Separator for clarity in logs

    if args.command == 'config':
        uim.update_config_from_json(force=args.force)
        print("IMSI Config")

    if args.command == 'reload':
        uim.reload_config_from_source(force=args.force)
        print("IMSI Update")

    if args.command == 'set':
        print("IMSI set")
        if  (args.file or args.selections or args.options):
            uim.set_selections(args.file, args.selections, args.options, force=args.force)
        else:
            set.print_help()
            
    #if args.command == 'qstat':
    #    print("IMSI qstat")
    #    scheduler_tools.pbs_q_query(user=args.user)

    if args.command == 'list':
        print("IMSI list")
        if not (args.experiments or args.models or args.platforms or args.compilers or args.options):
            print("\n**ERROR: You must select at least one category to list:** \n")
            list.print_help()
        else:
            uim.list_choices(args.repo, args.experiments, args.models, args.platforms, args.compilers, args.options)  

    if args.command == 'build':
        print("IMSI Build")
        uim.compile_model_execs()  

    if args.command == 'submit':
        print("IMSI submit")
        uim.submit_run()  

    if not args.command:
        parser.print_help()

if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
